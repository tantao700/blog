package io.bloom.site.service.impl;

import io.bloom.site.service.ArchiveService;
import org.nutz.ioc.loader.annotation.IocBean;

/**
 * ArchiveServiceImpl
 *
 * @author T_T
 * @since 26/11/2017 22:08
 */
@IocBean(name = "archiveService")
public class ArchiveServiceImpl implements ArchiveService {
}
