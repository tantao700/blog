package io.bloom.site.service.impl;

import io.bloom.site.service.OptionService;
import org.nutz.ioc.loader.annotation.IocBean;

/**
 * OptionServiceImpl
 *
 * @author T_T
 * @since 26/11/2017 22:08
 */
@IocBean(name = "optionService")
public class OptionServiceImpl implements OptionService {
}
