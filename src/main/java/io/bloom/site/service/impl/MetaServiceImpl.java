package io.bloom.site.service.impl;

import io.bloom.site.service.MetaService;
import org.nutz.ioc.loader.annotation.IocBean;

/**
 * MetaServiceImpl
 *
 * @author T_T
 * @since 26/11/2017 22:08
 */
@IocBean(name = "metaService")
public class MetaServiceImpl implements MetaService {
}
