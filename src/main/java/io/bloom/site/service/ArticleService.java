package io.bloom.site.service;

import io.bloom.site.model.ArticleDO;

/**
 * ArticleService
 *
 * @author T_T
 * @since 19/11/2017 17:07
 */
public interface ArticleService {

    void create(ArticleDO articleDO);

}
