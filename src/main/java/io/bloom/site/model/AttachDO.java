package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Table;

/**
 * AttachDO
 *
 * @author T_T
 * @since 18/11/2017 20:33
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_attach")
public class AttachDO extends AbstractDO {
    String name;
    String type;
    String key;
    Integer userId;
}
