package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Table;

/**
 * ArticleDO
 *
 * @author T_T
 * @since 15/11/2017 20:31
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_article")
public class ArticleDO extends AbstractDO{

    /**
     * 内容标题
     */
    @Column
    String title;

    @Column
    String slug;

    @Column
    String content;

    // 内容所属用户id
    @Column
    Long userId;

    /**
     * 点击次数
     */
    @Column
    Integer hits;

    /**
     * 内容类别
     */
    @Column
    String type;

    /**
     * 内容类型，markdown或者html
     */
    @Column
    String fmtType;

    /**
     * 文章缩略图
     */
    @Column
    String thumbImg;

    /**
     * 标签列表
     */
    @Column
    String tags;

    // 分类列表
    @Column
    String categories;

    // 内容状态
    @Column
    String status;

    // 内容所属评论数
    @Column
    Integer commentsNum;
    // 是否允许评论
    @Column
    Boolean allowComment;
    // 是否允许ping
    @Column
    Boolean allowPing;
    // 允许出现在聚合中
    @Column
    Boolean allowFeed;
}
