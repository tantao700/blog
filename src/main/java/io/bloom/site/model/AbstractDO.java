package io.bloom.site.model;

import lombok.Data;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * Activity
 *
 * @author T_T
 * @since 9/18/15
 */
@Data
public abstract class AbstractDO implements Serializable {

    @Id
    @Column("id")
    protected Long id;

    @Column
    protected Date created;

    @Column
    protected Date modified;
}
