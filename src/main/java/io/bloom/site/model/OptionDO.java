package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

/**
 * OptionDO
 *
 * @author T_T
 * @since 26/11/2017 21:36
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_option")
public class OptionDO {

    /**
     * 配置名称
     */
    @Name
    String name;

    /**
     * 配置值
     */
    String value;

    /**
     * 配置描述
     */
    String description;
}
