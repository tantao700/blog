package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Table;

/**
 * LogDO
 *
 * @author T_T
 * @since 18/11/2017 20:34
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_log")
public class LogDO extends AbstractDO {

    /**产生的动作*/
     String action;

    /**产生的数据*/
     String data;

    /**发生人id*/
     Integer userId;

    /**日志产生的ip*/
     String ip;
}
