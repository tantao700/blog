package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Table;

/**
 * MetaDO
 *
 * @author T_T
 * @since 26/11/2017 21:31
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_meta")
public class MetaDO {

    /**项目主键*/
    @Column
    Long id;

    /**项目类型*/
    String  type;

    /**选项描述*/
    String  value;

    /**父级*/
    Long parent;
}
