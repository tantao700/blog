package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.Table;

/**
 * Comment
 *
 * @author T_T
 * @since 18/11/2017 20:28
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_comment")
public class CommentDO extends AbstractDO {

    /**
     * post表主键,关联字段
     */
    Long postId;

    /**
     * 评论作者
     */
    String author;

    /**
     * 评论所属用户id
     */
    Long userId;

    /**
     * 评论所属内容作者id
     */
    Long ownerId;

    /**
     * 评论者邮件
     */
    String mail;

    /**
     * 评论者网址
     */
    String url;

    /**
     * 评论者ip地址
     */
    String ip;

    /**
     * 评论者客户端
     */
    String agent;

    /**
     * 评论内容
     */
    String content;

    /**
     * 评论类型
     */
    String type;

    /**
     * 评论状态
     */
    String status;

    /**
     * 父级评论
     */
    Long parentId;
}
