package io.bloom.site.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

/**
 * Activity
 *
 * @author T_T
 * @since 9/21/15
 */
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("t_user")
public class UserDO extends AbstractDO {

    /**
     * 用户昵称
     */
    @Column
    @ColDefine(width = 50)
    String username;

    @Column
    @ColDefine(width = 255)
    String password;

    @Column
    @ColDefine(width = 255)
    String email;

    /**
     * 用户的主页
     */
    String homeUrl;

    /**
     * 用户显示的名称
     */
    String screenName;

    /**
     * 上次登录最后活跃时间
     */
    Integer logged;

    /**
     * 用户组
     */
    String groupName;
}
