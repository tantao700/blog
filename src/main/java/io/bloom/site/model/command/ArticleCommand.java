package io.bloom.site.model.command;

import io.bloom.site.model.ArticleDO;
import io.bloom.site.model.MetaDO;
import lombok.Data;

import java.util.List;

/**
 * ArticleCommand
 *
 * @author T_T
 * @since 27/11/2017 22:20
 */
@Data
public class ArticleCommand {

    ArticleDO article;

    List<MetaDO> tags;

    List<MetaDO> categories;

}
