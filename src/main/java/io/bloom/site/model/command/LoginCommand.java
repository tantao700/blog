package io.bloom.site.model.command;

import lombok.Data;
import org.nutz.plugins.validation.annotation.Validations;

/**
 * LoginCommand
 *
 * @author T_T
 * @since 18/11/2017 21:19
 */
@Data
public class LoginCommand {

    @Validations(required = true, errorMsg = "创意名称不可为空")
    String username;

    @Validations(required = true,errorMsg ="创意名称不可为空")
    String password;

    String rememberMe;
}
