package io.bloom.site.model.command;

/**
 * Activity
 *
 * @author T_T
 * @since 9/20/15
 */
public interface Command<T> {

    T apply();
}
