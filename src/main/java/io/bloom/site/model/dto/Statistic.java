package io.bloom.site.model.dto;

import lombok.Data;

/**
 * Statistic
 *
 * @author T_T
 * @since 18/11/2017 22:55
 */
@Data
public class Statistic implements java.io.Serializable{

    /**
     * 文章数
     */
    long articles;
    /**
     * 页面数
     */
    long pages;
    /**
     * 评论数
     */
    long comments;
    /**
     * 分类数
     */
    long categories;
    /**
     * 标签数
     */
    long tags;
    /**
     * 附件数
     */
    long attachs;
}
