package io.bloom.site;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * MainStarter
 *
 * @author T_T
 * @since 17/11/2017 22:34
 */
@Slf4j
public class MainStarter {

    public static void main(String[] args) {
        // 服务器的监听端口
        Server server = new Server(9999);
        // 关联一个已经存在的上下文
        WebAppContext context = new WebAppContext();
        // 设置描述符位置
        System.out.println(MainStarter.class.getResource("/webapp"));
        context.setDescriptor(MainStarter.class.getResource("/webapp/WEB-INF/web.xml").toExternalForm());
        // 设置Web内容上下文路径
        context.setResourceBase(MainStarter.class.getResource("/webapp").toExternalForm());
        // 设置上下文路径
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        server.setHandler(context);

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            log.error("boot server error", e);
        }

    }
}
