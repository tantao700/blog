package io.bloom.site.exception;

/**
 * runtime exception
 *
 * @author T_T
 * @since 9/18/15
 */
public class BizRTException extends RuntimeException {

    public BizRTException() {
        super();
    }

    public BizRTException(String message) {
        super(message);
    }

    public BizRTException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizRTException(Throwable cause) {
        super(cause);
    }
}
