package io.bloom.site.exception;

/**
 * Activity
 *
 * @author T_T
 * @since 9/18/15
 */
public class BizException extends Exception{
    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }
}
