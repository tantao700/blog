package io.bloom.site.module;

import org.nutz.mvc.annotation.*;
import org.nutz.mvc.ioc.provider.ComboIocProvider;
import org.nutz.plugins.view.ResourceBundleViewResolver;

/**
 * Activity
 *
 * @author T_T
 * @since 9/18/15
 */
@Modules(scanPackage = true)
@SetupBy(MainSetup.class)
@IocBy(type = ComboIocProvider.class, args = {
        "*js"
        , "ioc/"
        , "*anno"
        , "io.bloom.site.module"
        , "io.bloom.site.service"
        , "*tx"
})
@Ok("json:full")
@Fail("jetx:500")
@Localization(value="msg/", defaultLocalizationKey="zh-CN")
@Views({ResourceBundleViewResolver.class})
public class MainModule {
}
