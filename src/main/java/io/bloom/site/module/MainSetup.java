package io.bloom.site.module;

import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.ioc.Ioc;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;

/**
 * Activity
 *
 * @author T_T
 * @since 9/18/15
 */
public class MainSetup implements Setup {


    @Override
    public void init(NutConfig conf) {
        //驼峰 转 下划线
        Daos.FORCE_HUMP_COLUMN_NAME = true;
        Ioc ioc = conf.getIoc();
        Daos.createTablesInPackage(ioc.get(Dao.class), "io.bloom.site.model", true);
    }

    @Override
    public void destroy(NutConfig nutConfig) {

    }

}
