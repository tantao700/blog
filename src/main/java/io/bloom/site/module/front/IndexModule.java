package io.bloom.site.module.front;

import io.bloom.site.model.ArticleDO;
import io.bloom.site.model.AttachDO;
import io.bloom.site.model.dto.Page;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * IndexModule
 *
 * @author T_T
 * @since 15/11/2017 20:57
 */
@IocBean
public class IndexModule {

    @At(value = "/index", top = true, methods = "get")
    @Ok("jetx:index")
    public Map<String, Object> temp() {
        return new HashMap<String, Object>() {
            {
                put("a", "a");
                put("b", "b");
            }
        };
    }

    @At(value = "/", top = true, methods = "get")
    @Ok("jetx:front.default.index")
    public Page<ArticleDO> index() {
        Page<ArticleDO> page = new Page<>();

        ArticleDO attachDO = new ArticleDO();
        attachDO.setSlug("slug");
        attachDO.setContent("content");
        page.setRows(Arrays.asList(attachDO));

        return page;
    }
}
