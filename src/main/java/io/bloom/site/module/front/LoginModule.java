package io.bloom.site.module.front;

import io.bloom.site.model.UserDO;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.View;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.view.ServerRedirectView;
import org.nutz.web.util.Sessions;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Activity
 *
 * @author T_T
 * @since 9/21/15
 */
@IocBean
public class LoginModule {

    @Inject
    private Dao dao;

    @At(value = "/login", top = true, methods = "post")
    public View doLogin(@Param("username") String username, @Param(value = "password",df = "") String password, HttpSession session) {

        UserDO query = dao.fetch(UserDO.class, Cnd.where("username", "=", "szt"));

        if (password.equals(query.getPassword())) {
            session.setAttribute("me", "管理员");

            String _go = Sessions.takeOrDefaultAttr(session, "_go","/admin");
            return new ServerRedirectView(_go);
        }
        return new ServerRedirectView("/login?username=" + username);
    }

    @At(value = "/login", top = true, methods = "get")
    @Ok("jsp:login")
    public void login() {}

    @At(value = "/logout", top = true)
//    @Ok(">>:/")
    public View logut(HttpSession session,@Param(value = "go",df = "/")String go) throws IOException {
        session.invalidate();
        return new ServerRedirectView(go);

    }
}
