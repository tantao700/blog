package io.bloom.site.module.front;

import io.bloom.site.model.ArticleDO;
import lombok.extern.slf4j.Slf4j;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

/**
 * ArticleModule
 *
 * @author T_T
 * @since 26/11/2017 23:07
 */
@Slf4j
@IocBean
@At("/article")
public class ArticleModule {

    @At(value = "/?/?", methods = "get")
    @Ok("jetx:front.default.article")
    public ArticleDO index(Long id, String title) {
        log.info("id:{},title:{}", id, title);
        return new ArticleDO();
    }

}
