package io.bloom.site.module.admin;

import io.bloom.site.model.UserDO;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * AdminSettingModule
 *
 * @author T_T
 * @since 19/11/2017 15:59
 */
@IocBean
@At("/admin/setting")
//@Filters(@By(type = AuthorizeFilter.class)) // 检查当前Session是否带me这个属性
public class AdminSettingModule {

    @At(value = {"/"},methods = "get")
    @Ok("jetx:admin.setting")
    public Map<String,Object> index(HttpSession session) {
        UserDO userDO=new UserDO();
        session.setAttribute("user",userDO);
        return new HashMap<>();
    }
}
