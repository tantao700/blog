package io.bloom.site.module.admin;

import io.bloom.site.model.AttachDO;
import io.bloom.site.model.UserDO;
import io.bloom.site.model.dto.Page;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import javax.servlet.http.HttpSession;

/**
 * AdminAttachModule
 *
 * @author T_T
 * @since 19/11/2017 16:23
 */
@IocBean
@At("/admin/attach")
public class AdminAttachModule {

    @At(value = {"/"},methods = "get")
    @Ok("jetx:admin.attach")
    public Page<AttachDO> index(HttpSession session) {
        UserDO userDO=new UserDO();
        session.setAttribute("user",userDO);
        return new Page<>();
    }
}
