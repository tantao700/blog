package io.bloom.site.module.admin;

import io.bloom.site.model.UserDO;
import io.bloom.site.model.dto.Statistic;
import io.bloom.site.service.UploadService;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.json.Json;
import org.nutz.lang.Files;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.upload.TempFile;
import org.nutz.mvc.upload.UploadAdaptor;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 管理员模块
 *
 * @author T_T
 * @since 9/20/15
 */
@IocBean
@At("/admin")
//@Filters(@By(type = AuthorizeFilter.class)) // 检查当前Session是否带me这个属性
public class AdminModule {

    @Inject("uploadService")
    protected UploadService uploadService;


    @Inject
    protected Dao dao;


    //##############[dashboard]##################
    @At({"/", "", "/dashboard"})
    @Ok("jetx:admin.index")
    public void dashboard(HttpServletRequest request) {
        request.setAttribute("user",new UserDO(){
            @Override
            public String getUsername() {
                return "username_ok";
            }

            @Override
            public String getPassword() {
                return "password_ok";
            }

            @Override
            public String getEmail() {
                return "email_ok";
            }
        });
        request.setAttribute("statistics",new Statistic(){
            @Override
            public long getArticles() {
                return 100;
            }
        });
    }


    //##############[news-image-upload]##################


    @At(value = "/news-image-upload")
    @AdaptBy(type = UploadAdaptor.class, args = {"${app.root}/WEB-INF/tmp"})
    public String newsImageUpload(@Param("image") TempFile f) throws IOException {
        return uploadService.move(f.getFile());
    }

    @At(value = "/news-image-link-upload")
    @AdaptBy(type = UploadAdaptor.class, args = {"${app.root}/WEB-INF/tmp"})
    public String newsImageLinkUpload(@Param("imageLink") TempFile f) throws IOException {
        return uploadService.move(f.getFile());
    }

    
    //##############[job-module]##################
    @At(value = "/job-list", methods = "get")
    @Ok("jsp:admin.job-list")
    public void jobList() {
    }


    @At(value = "/ueditor-upload")
    @Ok("raw:html")
    @AdaptBy(type = UploadAdaptor.class, args = {"${app.root}/WEB-INF/tmp"})
    public String ueditorUpload(@Param("imageLink") TempFile f) throws IOException {
        long fileLength = f.getFile().length();
        String move = uploadService.move(f.getFile());
        String name = f.getSubmittedFileName();
        NutMap map = NutMap.NEW()
                .addv("original", name)
                .addv("name", name)
                .addv("url", move)
                .addv("size", fileLength)
                .addv("type", Files.getSuffixName(name))
                .addv("state", "SUCCESS");
        return Json.toJson(map);
    }
}
