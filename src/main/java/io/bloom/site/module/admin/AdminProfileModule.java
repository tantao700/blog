package io.bloom.site.module.admin;

import io.bloom.site.model.UserDO;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import javax.servlet.http.HttpSession;

/**
 * AdminProfileModule
 *
 * @author T_T
 * @since 19/11/2017 15:51
 */
@IocBean
@At("/admin/profile")
//@Filters(@By(type = AuthorizeFilter.class)) // 检查当前Session是否带me这个属性
public class AdminProfileModule {

    @At(value = {"/"},methods = "get")
    @Ok("jetx:admin.profile")
    public UserDO index(HttpSession session) {
        UserDO userDO=new UserDO();
        session.setAttribute("user",userDO);
        return userDO;
    }

}
