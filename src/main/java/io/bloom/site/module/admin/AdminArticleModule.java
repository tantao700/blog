package io.bloom.site.module.admin;

import io.bloom.site.model.ArticleDO;
import io.bloom.site.model.UserDO;
import io.bloom.site.model.command.ArticleCommand;
import io.bloom.site.model.dto.Page;
import io.bloom.site.service.ArticleService;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpSession;

/**
 * AdminArticleModule
 *
 * @author T_T
 * @since 18/11/2017 23:10
 */
@IocBean
@At("/admin/article")
//@Filters(@By(type = AuthorizeFilter.class)) // 检查当前Session是否带me这个属性
public class AdminArticleModule {

    @Inject
    ArticleService articleService;

    @At(value = {"/modify","/save","/publish"},methods = "get")
    @Ok("jetx:admin.article-edit")
    public void edit(HttpSession session) {
        session.setAttribute("user", new UserDO());
    }

    @At(value = {"/modify","/save","/publish"},methods = "post")
    @Ok("json")
    public void doSave(@Param("..")ArticleCommand command,HttpSession session) {
        UserDO user= (UserDO) session.getAttribute("user");

        ArticleDO article = command.getArticle();
        article.setType("post");
        article.setUserId(user.getId());

        if (Strings.isEmpty(article.getCategories())) {
            article.setCategories("默认分类");
        }

        articleService.create(article);

    }

    @At(value = {"/list"},methods = "get")
    @Ok("jetx:admin.article-list")
    public Page<ArticleDO> list(HttpSession session) {
        session.setAttribute("user", new UserDO());
        return new Page<>();
    }
}
