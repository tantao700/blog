package io.bloom.site.module.admin;

import io.bloom.site.model.command.LoginCommand;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import javax.servlet.http.HttpSession;

import static io.bloom.site.util.AppUtil.isLogin;

/**
 * AdminLoginModule
 *
 * @author T_T
 * @since 18/11/2017 21:05
 */
@IocBean
@At("/admin/login")
public class AdminLoginModule {

    @At(value = {"/", ""}, methods = "get")
    @Ok("re:jetx:admin.login")
    public String login(HttpSession session) {
        if (isLogin(session)) {
            return ">>:/admin";
        }
        return null;
    }


    @At(value = {"/", ""}, methods = "post")
    @Ok(">>:/admin")
    public void doLogin(LoginCommand command,HttpSession session) {
        session.setAttribute("user",command);
    }
}
