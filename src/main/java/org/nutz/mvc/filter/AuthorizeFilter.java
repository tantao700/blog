package org.nutz.mvc.filter;

import org.nutz.mvc.ActionContext;
import org.nutz.mvc.ActionFilter;
import org.nutz.mvc.Mvcs;
import org.nutz.mvc.View;
import org.nutz.mvc.view.ServerRedirectView;

import javax.servlet.http.HttpSession;

/**
 * Activity
 *
 * @author T_T
 * @since 9/22/15
 */
public class AuthorizeFilter implements ActionFilter {

    private String loginUrl = "/login";

    public AuthorizeFilter() {
    }

    @Override
    public View match(ActionContext context) {

        //如果是登录页面就走
        String accessPath = context.getPath();

        if (loginUrl.equals(accessPath)) {
            return null;
        }

        //如果没有
        HttpSession httpSession = Mvcs.getHttpSession();
        if (httpSession.getAttribute("me") == null) {
            httpSession.setAttribute("_go", accessPath);
            return new ServerRedirectView(loginUrl);
        }

        return null;
    }
}
