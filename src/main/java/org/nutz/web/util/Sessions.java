package org.nutz.web.util;

import javax.servlet.http.HttpSession;

/**
 * Activity
 *
 * @author T_T
 * @since 9/22/15
 */
public class Sessions {

    @SuppressWarnings("unchecked")
    public static <T> T attr(HttpSession session,String attrName){
        return (T) session.getAttribute(attrName);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getOrDefaultAttr(HttpSession session,String attrName,T defaultValue){
        T value = (T) session.getAttribute(attrName);
        return value==null?defaultValue:value;
    }


    public static <T> T takeOrDefaultAttr(HttpSession session, String attrName,T defaultValue){
        try {
            T value = getOrDefaultAttr(session, attrName,defaultValue);
            return value==null?defaultValue:value;
        } finally {
            session.removeAttribute(attrName);
        }
    }
}
