package org.nutz.web.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Activity
 *
 * @author T_T
 * @since 9/23/15
 */
public class Requests {
    @SuppressWarnings("unchecked")
    public static <T> T attr(HttpServletRequest request,String attrName){
        return (T) request.getAttribute(attrName);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getOrDefaultAttr(HttpServletRequest request,String attrName,T defaultValue){
        T value = (T) request.getAttribute(attrName);
        return value==null?defaultValue:value;
    }


}
