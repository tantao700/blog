var ioc = {
    dataSource: {
        type: "com.alibaba.druid.pool.DruidDataSource",
        events: {
            create: "init",
            depose: 'close'
        },
        fields: {
            //url : "jdbc:mysql://127.0.0.1:3306/nutz",
            driverClassName: "org.h2.Driver",
            url: "jdbc:h2:~/blog",
            username: "sa",
            password: "",
            testWhileIdle: true,
            validationQuery: "select 1",
            maxActive: 100
        }
    },
    dao: {
        type: "org.nutz.dao.impl.NutDao",
        args: [{refer: "dataSource"}]
    },
    config: {
        type: "org.nutz.ioc.impl.PropertiesProxy",
        fields: {
            paths: ["application.properties"]
        }
    },
    jetx: {
        type: "org.nutz.plugins.view.JetTemplateView",
        args: [null],
        fields: {
            prefix: "/WEB-INF/views",
            suffix: ".jetx"
        }
    },
    multiViewResover: {
        type: "org.nutz.plugins.view.MultiViewResover",
        fields: {
            resolvers: {
                "jetx": {
                    refer: "jetx"
                }
            }
        }
    },
    authorizeFilter: {
        type: "org.nutz.mvc.filter.AuthorizeFilter",
        args: ["/login"]
    }
};